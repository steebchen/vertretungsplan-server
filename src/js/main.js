$(document).ready(function() {
	$('#cancelModal').submit(function(e) {
		e.preventDefault();

		$.ajax({
			url: '/add',
			method: 'post',
			data: $(this).serialize()
		}).then(function(result, status) {
			if (result.error) {
				return alert(result.error); // TODO
			}

			$('#cancelModal').closeModal();
			location.reload();
		});

		return false;
	});

	$('#messageModal').submit(function(e) {
		e.preventDefault();

		$.ajax({
			url: '/addMessage',
			method: 'post',
			data: $(this).serialize()
		}).then(function(result, status) {
			if (result.error) {
				return alert(result.error); // TODO
			}

			$('#messageModal').closeModal();
			location.reload();
		});

		return false;
	});

	$('[data-tooltip]').tooltip({
		delay: 10
	});

	$('#cancel').click(function() {
		$('#cancelModal').openModal();
	});

	$('#message').click(function() {
		$('#messageModal').openModal();
	});

	$('select').material_select();

	var $datepickers = $('.datepicker');
	$datepickers.pickadate({
		selectMonths: true,
		selectYears: 2
	});

	var $datepickerWrapper = $('.datepicker-wrapper');

	$('.reset-choose').click(function() {
		$(this).nextAll('.datepicker-wrapper').removeClass('shown');
	});

	$('.choose').click(function() {
		$datepickerWrapper.addClass('shown');
	});
});
