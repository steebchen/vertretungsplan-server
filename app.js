require('../helpers/default')

var Issue = require('./models/Issue')

var app = express.init({
	name: 'RepresentationPlan',
	session: {
		secret: 'djo8923'
	}
})

global.hours = [
	'1/2', '3/4', '5/6', '8/9', '10/11',
	'1', '2', '3', '4', '5', '6', '(7)', '8', '9', '10', '11'
]

global.classes = [
	'E1', 'E2', 'E3', 'E4', 'E5',
	'J1.1', 'J1.2', 'J1.3', 'J1.4', 'J1.5',
	'J2.1', 'J2.2', 'J2.3', 'J2.4', 'J2.5'
]

global.courses = [{
	id: 'economy',
	name: 'Wirtschaft',
	items: [{
		id: 'W1',
		teacher: 'Noetzelmann',
		notes: 'v'
	}, {
		id: 'W2',
		teacher: 'Noetzelmann',
		notes: 'n'
	}, {
		id: 'W3',
		teacher: 'Beck',
		notes: 'v'
	}, {
		id: 'W4',
		teacher: 'Beck',
		notes: 'n'
	}]
}, {
	id: 'sports',
	name: 'Sport',
	items: [{
		id: 'S1',
		teacher: 'Müller',
		notes: 'v'
	}, {
		id: 'S2',
		teacher: 'Müller',
		notes: 'n'
	}, {
		id: 'S3',
		teacher: 'Ka',
		notes: 'v'
	}, {
		id: 'S4',
		teacher: 'Ka',
		notes: 'n'
	}]
}]

app.use(function(req, res, next) {
	req.model.hours = hours
	req.user = req.session.user
	req.model.classes = classes
	req.model.courses = courses

	next()
})

app.get('/package.json', function(req, res, next) {
	res.sendJSON({
		"name": "Vertretungsplan",
		"version": "0.0.0",
		"description": "Vertretungsplan Administratoroberfläche",
		"main": "app.js",
		"author": {
			"name": "Luca Steeb",
			"email": "contact@luca-steeb.com"
		},
		"dependencies": {
			"body-parser": "1.12.2",
			"bugsnag": "^1.7.0",
			"cookie-parser": "^1.4.0",
			"express": "^4.13.4",
			"express-session": "^1.13.0",
			"ionic-push-server": "^1.1.1",
			"moment": "^2.9.0",
			"mongoose": "^4.0.2",
			"pm2": "^1.1.1",
			"request": "*",
			"serve-static": "^1.10.2",
			"vash": "^0.7.12-1",
		}
	})
})

app.get('/json', function(req, res, next) {
	res.send({
		name: "Hans",
		hobbys: ["essen", "trinken"],
		daten: {
			alter: 18,
			geschlecht: "männlich"
		}
	})
})

app.get('/', function(req, res, next) {

	if (!req.user) {
		return res.redirect('/login')
	}

	var query = {}

	if (!req.query.all) {
		query.date = {
			$gt: new Moment().startOf('day').subtract(1, 'day').valueOf()
		}
	}

	Issue.find(query).sort({
		date: 1
	}).exec((err, issues) => {
		if (err) return next(err)

		req.model.issues = issues

		res.render('index', req.model)
	})
})

app.get('/login', function(req, res, next) {
	if (req.user) {
		return res.redirect('/')
	}

	res.render('login', req.model)
})

app.post('/login', function(req, res, next) {
	console.log(req.body)

	if (req.body.username !== 'a' && req.body.password !== 'b') {
		req.model.message = {
			title: 'Fehler',
			message: 'Benutzer oder Passwort falsch',
			code: 'invalid'
		}

		return res.render('login', req.model)
	}

	req.session.user = {
		username: req.body.username
	}

	res.redirect('/')
})

app.get('/api/getIssues', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', '*')

	User.findOne({
		token: req.query.token
	}).exec((err, user) => {
		if (err) return next(err)

		console.log(user)

		if (!user) {
			console.log('no user found');
			return res.send([])
		}

		var query = {
			date: {
				$gt: new Moment().startOf('day').subtract(1, 'day').valueOf()
			}
		}

		query.classes = user.class

		console.log('/getIssues query:', query)

		Issue.find(query).sort({
			date: 1
		}).lean().exec(function(err, result) {
			if (err) return next(err)

			var today = new Moment().startOf('day').valueOf(),
					tomorrow = new Moment().startOf('day').add(1, 'days').valueOf()

			result.forEach(item => {
				if (new Moment(item.date).startOf('day').valueOf() === today) {
					item.date = 'Heute'
				} else if (new Moment(item.date).startOf('day').valueOf() === tomorrow) {
					item.date = 'Morgen'
				} else {
					item.date = 'am ' + new Moment(item.date).format('DD.MM.YYYY')
				}
			})

			res.send(result)
		})
	})
})

app.get('/api/getAllIssues', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', '*')

	Issue.find().exec(function(err, result) {
		if (err) return next(err)

		res.send(result)
	})
})

app.get('/api/getUser', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', '*')

	User.findOne({
			token: req.query.token
	}).exec((err, user) => {
		if (err) return next(err)

		if (user) {
			return res.send({
				success: true,
				user: user
			})
		}

		res.send({
			success: false
		})
	})
})

app.get('/api/createUser', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', '*')

	User.findOne({
			token: req.query.token
	}).exec((err, user) => {
		if (err) return next(err)

		if (user) {
			console.log('user already exists')
			return res.send({
				success: true,
				alreadyExists: true,
				user: user
			})
		}

		var user = new User({
			number: req.query.number,
			class: req.query.class,
			token: req.query.token,
			courses: req.query.courses
		})

		user.save(function(err) {
			if (err) return next(err)

			res.send({
				success: true
			})
		})
	})
})

express.listen(app)
