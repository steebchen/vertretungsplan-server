'use strict'

var ionicPushServer = require('ionic-push-server');

var credentials = {
	IonicApplicationID: '45f7c98f',
	IonicApplicationAPItoken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkMTliZjdjNC1jYTQ1LTRmNjItYWQ2MC03ZTQwZjIzZGViYjIifQ.o4Nk6C-Cq771jWmRVRiw2LQFZC2VsW2QYdHd-qofd2Y'
}

var app = new express.Router()

function handleHours(req, res, next) {
	// console.log('hours before:', req.body.hours)
	//
	// req.body.hours = req.body.hours || []
	//
	// if (typeof req.body.hours === 'string') {
	// 	req.body.hours = [req.body.hours]
	// }
	//
	// req.body.hours.forEach((item) => {
	// 	if (~item.indexOf('/')) {
	// 		let v = req.body.hours[item.indexOf('/')]
	//
	// 		console.log('splitting', req.body.hours[item.indexOf('/')], 'in parts')
	//
	// 		req.body.hours.push(v[0], v[1])
	// 		req.body.hours.splice(item.indexOf('/'), 1)
	// 	}
	// })

	if (~req.body.hours.indexOf('day')) {
		req.body.hours = ['Gesamter Tag']
	}

	if (~req.body.hours.indexOf('forenoon')) {
		req.body.hours = ['Vormittag']
	}

	if (~req.body.hours.indexOf('afternoon')) {
		req.body.hours = ['Nachmittag']
	}
}

function handleClasses(req, res, next) {

	req.body.classes = req.body.classes || []
	req.body.courses = req.body.courses || []

	if (typeof req.body.classes === 'string') {
		req.body.classes = [req.body.classes]
	}

	if (typeof req.body.courses === 'string') {
		req.body.courses = [req.body.courses]
	}

	if ((req.body.classes.length === 0 || ~req.body.classes.indexOf('--')) &&
		(req.body.courses.length === 0 || ~req.body.courses.indexOf('--'))) {
		return res.send({
			error: 'Es muss mindestens eine Klasse oder ein Kurs ausgewählt werden.'
		})
	}

	let multiple = []

	console.log('classes:', req.body.classes)

	if (~req.body.classes.indexOf('E')) {
		multiple.push('E')
	}

	if (~req.body.classes.indexOf('J1')) {
		multiple.push('J1')
	}

	if (~req.body.classes.indexOf('J2')) {
		multiple.push('J2')
	}

	if (~req.body.classes.indexOf('all')) {
		req.body.classes = global.classes
	}

	for (let m of multiple) {
		if (m) {
			for (let c of classes) {
				if (~c.indexOf(m)) {
					req.body.classes.push(c)
				}
			}

			req.body.classes.splice(req.body.classes.indexOf(m), 1)
		}
	}

	next()
}

app.post('/addMessage', handleClasses, (req, res, next) => {
	if (!req.body.message) {
		return res.send({
			error: 'Es wurde keine Nachricht gewählt.'
		})
	}

	if (req.body.when === 'now') {
		req.body.date = new Moment().startOf('day')
	}

	try {
		req.body.date = new Moment(new Date(req.body.date))
	} catch (e) {
		return res.send({
			error: 'Datum wurde nicht korrekt ausgewählt'
		})
	}

	req.body.type = 'message'

	var issue = new Issue(req.body)

	issue.save((err) => {
		if (err) return next(err)

		res.send({
			success: true
		})

		sendNotification(issue, req.body.title || 'Neue Nachricht', req.body.message)
	})
})

app.post('/add', handleClasses, (req, res, next) => {
	console.log(req.body)

	if (req.body.when === 'choose') {
		try {
			req.body.date = new Moment(new Date(req.body.date))
		} catch (e) {
			return res.send({
				error: 'Datum wurde nicht korrekt ausgewählt'
			})
		}
	} else {
		req.body.date = new Moment().startOf('day')

		if (req.body.when === 'tomorrow') {
			req.body.date.add(1, 'days')
		}
	}

	if (!req.body.hours || req.body.hours.length === 0) {
		return res.send({
			error: 'Es muss mindestens ein Eintrag unter "Stunde" gewählt werden'
		})
	}

	var issue = new Issue(req.body)

	issue.save((err) => {
		if (err) return next(err)

		res.send({
			success: true
		})

		let date = req.body.date
		let today = new Moment().startOf('day').valueOf()
		let time = ''

		if (date.startOf('day').valueOf() === today) {
			time = 'Heute'
		} else if (date.startOf('day').add(1, 'days').valueOf() === today) {
			time = 'Morgen'
		} else {
			// program minifcation
			//time = 'Am ' + req.body.date.format('DD.MM')
			return console.log('issue.date is after tomorrow')
		}

		sendNotification(
			issue,
			'Neuer Ausfall',
			time + ' fallen die Stunden ' + issue.hours.join(', ') + ' aus. :)'
		)
	})

	//res.redirect('/')
})

function sendNotification(issue, title, message) {
	// if (issue.date.isAfter(new Moment().startOf('day').add(1, 'days'))) {

	// }


	let array =
		issue.classes.length > 0 ?
			issue.classes.map(item => {
				return {
					class: item
				}
			}) :
			issue.courses.map(item => {
				return {
					courses: item
				}
			})

	console.log('array:', array);

	let query = {
		$or: array
	}

	console.log('query:', query)

	User.find(query).exec((err, result) => {
		if (err) return logger.error(err)

		console.log('result:', result)

		if (result.length === 0) return

		let notification = {
			tokens: result.map(item => item.token),
			profile: 'default',
			notification: {
				title: title,
				message: message,
				// android: {
				// },
				// ios: {
				// }
			}
		}

		console.log('sending to tokens:', notification.tokens)

		ionicPushServer(credentials, notification)
	})
}

module.exports = app
app
