var Schema = new mongoose.Schema({
	_id: {
		type: String,
		default: () => {
			return global.randomDateString()
		}
	},

	type: String, // replace vs cancel
	comment: String,
	date: 'Moment',
	hours: [String],
	classes: [String],
	courses: [String],

	// message:
	title: String,
	message: String,
});

module.exports = mongoose.model('rp-Issue', Schema);
