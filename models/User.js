var Schema = new mongoose.Schema({
	_id: {
		type: String,
		default: () => {
			return global.randomString()
		}
	},

	number: String,
	token: String,
	class: String,
	courses: [String]
});

module.exports = mongoose.model('rp-User', Schema);
